=== THIS PROJECT WILL BE DEPRECATED, USE `https://gitlab.com/jryannel/qauto-admin` ===


# QAuto Device

A set of scripts to enable the easy setup of QAuto (https://www.qt.io/qt-automotive-suite/) on a remote device running ubuntu.


# Bootstrap 

Install the script. it requires python and pip to be installed.

    curl -L http://bit.ly/qauto-script | sh


# Local QtAuto Build

    mkdir qauto
    cd qauto
    qauto --help

Installing qt5

    qauto qt5_init
    qauto qt5_config
    qauto qt5_build

Installing QtAuto Suite

    qauto init
    qauto build


To update the source tree and build qauto again

    qauto update
    qauto build

To clean the qauto build just issue a

    qauto clean

# Run Neptune3

To run neptune3 just type

    qauto neptune3

Tip: You can also combine commands: `qauto update build neptune3`. This will 
first update your source tree, then build qauto components and then 
run the neptune3 ui.

# Remote Setup

On the host machine enter

    qauto-remote init --host <ip> --user <user>

This will connect to he remote device (runnning an Ubuntu version) and install the qauto script.

## Control Device

On host

    qauto-remote shell --host <ip> --user <user>


On remote device

    qauto --help

When logged in on the remote device you can follow the local build instructions.
