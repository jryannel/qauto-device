#!/bin/sh

run_it () {
    set -e
    set -u
    exec 1>&2

    command -v pip >/dev/null 2>&1 || {
        echo "pip is required. Please install first."
        echo "on ubuntu: $ sudo apt install python-pip"
        echo "abort..."
        exit 1
    }

    command -v git >/dev/null 2>&1 || {
        echo "git is required. Please install first."
        echo "on ubuntu: $ sudo apt install git"
        echo "abort..."
        exit 1
    }

    command -v cmake >/dev/null 2>&1 || {
        echo "cmake is required. Please install first."
        echo "on ubuntu: $ sudo apt install cmake"
        echo "abort..."
        exit 1
    }

    echo "installing qauto-device package..."
    pip install --quiet --upgrade git+https://gitlab.com/jryannel/qauto-device.git

    echo "Installed qauto-remote - to manage remote device"
    echo "Installed qauto - to manage local qauto installation"
    echo ""
    echo "To learn more type"
    echo "  $ qauto --help"
    echo "  $ qauto-remote --help"
    echo ""
    echo "In case you can not load qauto, make sure the ~/.local/bin folder is "
    echo "in your search path. "
    echo "(see also https://pip.pypa.io/en/stable/user_guide/#user-installs)"
    
}

run_it