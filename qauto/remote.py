#!/usr/bin/env python
# Copyright (c) Pelagicore AB 2017

import click
import fabric.api as fab

# from fabric.api import run, execute, env, open_shell, local, sudo
from fabric.network import disconnect_all


def job(fn, ctx):

    fab.env.hosts = [ctx.obj['host']]
    fab.env.user = ctx.obj['user']
    fab.execute(fn)
    disconnect_all()


def do_shell():
    fab.open_shell()


def do_init():
    fab.sudo('apt-get install curl')
    fab.run('curl -L http://bit.ly/qauto-script | sh')


@click.group()
@click.option('--host')
@click.option('--user')
@click.pass_context
def cli(ctx, host, user):
    ctx.obj['host'] = host
    ctx.obj['user'] = user
    pass


@cli.command()
@click.pass_context
def shell(ctx):
    """opens a shell on remote device"""
    job(do_shell, ctx)


@cli.command()
@click.pass_context
def init(ctx):
    """installs the qauto script on the remote"""
    job(do_init, ctx)


@cli.command()
def init_ubuntu():
    """initializes ubuntu packages"""


@cli.command()
@click.pass_context
def push_ssh_key(ctx):
    fab.local('ssh-copy-id {0}@{1}'.format(ctx.obj['user'], ctx.obj['host']))


def main():
    try:
        cli(obj={})
    finally:
        disconnect_all()


if __name__ == '__main__':
    main()
